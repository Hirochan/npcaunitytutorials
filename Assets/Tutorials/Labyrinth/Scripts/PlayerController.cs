using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float MoveSpeed;
    public float RotateSpeed;

    public GameObject StartObject;
    public GameObject GoalObject;
    public Text text;

    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        var d_RotateSpeed = new Vector3(0f, Input.GetAxis("Horizontal"), 0f) * RotateSpeed * Time.fixedDeltaTime;
        var d_MoveSpeed = MoveSpeed * transform.forward * Input.GetAxis("Vertical") - rb.velocity;
        transform.Rotate(d_RotateSpeed);
        rb.AddForce(d_MoveSpeed, ForceMode.VelocityChange);
    }

    void Update()
    {
        if(transform.position.y < -1)
        {
            var tmp = StartObject.transform.position;
            tmp.y = 1;
            transform.position = tmp;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == GoalObject.gameObject.name)
        {
            text.text = "GOAL!!";
        }
    }
}
